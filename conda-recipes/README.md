# Conda package how to

```bash
cd recipes
conda build mergeBlast
```

```bash
mkdir -p ~/src/channel/
```

```bash
cp /home/sortion/.local/share/miniforge3/conda-bld/linux-64/lamme-mergeblast-0.1.0-h9bf148f_0.tar.bz2 ~/src/channel/linux-64/
```
(don't forget to adapt the build number.)

```bash
cd ~/src/
conda index ./channel
```

```bash
conda create -n lamme-test -c file:/home/$USER/src/channel lamme-mergeblast
```
