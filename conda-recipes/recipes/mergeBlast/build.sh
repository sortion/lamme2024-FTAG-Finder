#!/usr/bin/env bash
make install \
    CC="${CC}" \
    CFLAGS="${CFLAGS}" \
    LDFLAGS="${LDFLAGS}" \
    prefix="${PREFIX}"
