# Changelog

## _v0.1.2_ - Glorious gannet

### Added

- support for diamond

### Modified

- Avoid Snakemake script, for easier portability, in case of a python dependency: favor Snakemake shell with python CLI.

## _v0.1.1_ - Funky falcon

### Added

- detect TAGs: find TAGs from gene families and gene features positions

## _v0.1.0_ - Shy starling

### Added

- blastp - "all against all"
- mmseqs2 (uncomplete)
- mergeBlast
- find_homologs: conversion of blastp output into graph in ABC format
- clustering
    - single linkage
    - Markov Clustering (mcl)
