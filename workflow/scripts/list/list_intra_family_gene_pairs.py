#!/usr/bin/env python3
"""
Extract all pairs of genes from the same family from a mapping from genes to families
"""

import argparse

parser = argparse.ArgumentParser(
    description="Extract all pairs of genes from the same family from a mapping from genes to families"
)
parser.add_argument(
    "-i",
    "--input",
    type=str,
    help="Input file with the mapping from genes to families gene\\tfamily",
    required=True,
)
parser.add_argument(
    "-o",
    "--output",
    type=str,
    help="Output file with the pairs of genes from the same family",
    required=True,
)
args = parser.parse_args()

with open(args.input, "r") as input_file:
    gene_families = {}
    for line in input_file:
        gene, family = line.strip().split("\t")
        if family not in gene_families:
            gene_families[family] = []
        gene_families[family].append(gene)

with open(args.output, "w") as output_file:
    for family in gene_families:
        n = len(gene_families[family])
        for i in range(n - 1):
            gene_a = gene_families[family][i]
            for j in range(i + 1, n):
                gene_b = gene_families[family][j]
                output_file.write("\t".join([gene_a, gene_b, family]) + "\n")
