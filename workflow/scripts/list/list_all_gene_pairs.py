#!/usr/bin/env bash
"""
Extract all possible gene pairs from a list of genes
"""

import argparse


def generate_pairs(names: list[str]) -> list[tuple[str, str]]:
    return [(a, b) for i, a in enumerate(names) for b in names[i + 1 :]]


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-i", "--input", type=str, help="Input list of genes", required=True
    )
    parser.add_argument(
        "-o",
        "--output",
        type=str,
        help="Output list of all pairs of genes",
        required=True,
    )
    args = parser.parse_args()

    with open(args.input, "r") as f:
        names: list[str] = f.read().splitlines()
        if "" in names:
            names.remove("")
    unique_names = list(set(names))
    pairs: list[tuple[str, str]] = generate_pairs(unique_names)

    with open(args.output, "w") as f:
        f.writelines(["\t".join(pair) + "\n" for pair in pairs])


if __name__ == "__main__":
    main()
