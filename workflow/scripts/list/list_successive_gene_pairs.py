#!/usr/bin/env python3
"""
Successive gene pairs and local pairs

Extract all pairs of genes from whatever the family they belong to
provided they are located on the same chromosome seperated by exactly n spacer (gene, or other selected genomic feature present in the features table)
"""

import argparse

from typing import Generator


def extract_successive_pairs(
    features: list[list[str]],
    n: int,
    column_to_index: dict[str, int],
    same_family: bool,
    family_dict: dict = None,
) -> Generator[tuple[str, str], None, None]:
    """
    Extract all pairs of genes from whatever the family they belong to
    provided they are located on the same chromosome seperated by at most $n$ spacer (gene, or other selected genomic feature present in the features table)

    Args:
        features: list of features, each feature is a list of strings with the following columns (name, chromosome, strand, start, stop, type),  Note: I assume all features type to be considered (except chromosomes), and that all features are sorted by chromosome and start position.
        n: maximum number of spacers between the genes

    Returns:
        list of pairs of successive genes seperated by at most n genes
    """
    encountered_chromosomes = set()
    current_chromosome: None = ""
    for i, reference_feature in enumerate(features):
        reference_name = reference_feature[column_to_index["name"]]
        reference_chromosome = reference_feature[column_to_index["chromosome"]]
        reference_start = int(reference_feature[column_to_index["start"]])
        reference_type = reference_feature[column_to_index["type"]]
        if reference_type != "gene":
            continue
        if current_chromosome != reference_chromosome:
            if reference_chromosome not in encountered_chromosomes:
                encountered_chromosomes.add(reference_chromosome)
                current_chromosome = reference_chromosome
            else:
                raise ValueError(
                    f"Features are not sorted by chromosome, {reference_chromosome} is encountered more than once"
                )
        spacer_count: int = 0
        for j, feature in enumerate(features[i + 1 :]):
            feature_name = feature[column_to_index["name"]]
            if feature[column_to_index["chromosome"]] != reference_chromosome:
                break
            start = int(feature[column_to_index["start"]])
            if start < reference_start:
                raise ValueError(
                    f"Features are not sorted by start position on chromosome {reference_chromosome}"
                )
            if feature[column_to_index["type"]] == "gene" and spacer_count <= n:
                if not same_family or (
                    reference_name in family_dict
                    and feature_name in family_dict
                    and family_dict[reference_name] == family_dict[feature_name]
                ):
                    yield reference_name, feature_name
            spacer_count += 1
            if spacer_count >= n:
                break


def main():
    parser = argparse.ArgumentParser(
        "Extract successive pairs, regardless of the family they belong "
    )
    parser.add_argument(
        "-i",
        "--input",
        type=str,
        help="Input file with the gene / features positions, with the following columns (name, chromosome, strand, start, stop, type)",
        required=True,
    )
    parser.add_argument(
        "-o",
        "--output",
        type=str,
        help="Output file with the pairs of successive genes",
        required=True,
    )
    parser.add_argument(
        "--definitions",
        type=str,
        help="Comma-seperated list of number of spacers between the genes to consider, example: 0,1,5,10",
        required=True,
    )

    args = parser.parse_args()

    column_names = ["name", "chromosome", "strand", "start", "stop", "type"]
    column_to_index = {
        column_name: index for index, column_name in enumerate(column_names)
    }

    definitions = list(map(int, args.definitions.split(",")))

    # I assume the whole set of features is small enough to fit in memory
    with open(args.input, "r") as input_file:
        features = []
        for line in input_file:
            features.append(line.strip().split("\t"))
            assert len(features[-1]) == len(
                column_names
            ), f"Error: {line} does not contain the expected number of columns"

    # Sort the features by chromosome and start position
    features.sort(
        key=lambda x: (
            x[column_to_index["chromosome"]],
            int(x[column_to_index["start"]]),
        )
    )

    with open(args.output, "w") as output_file:
        output_file.write("gene_a\tgene_b\tdefinition\n")
        for n in definitions:
            print(n)
            for gene_a, gene_b in extract_successive_pairs(
                features, n, column_to_index, same_family=False
            ):
                output_file.write("\t".join([gene_a, gene_b, f"SP{n}"]) + "\n")


if __name__ == "__main__":
    main()
