#!/usr/bin/env python3
"""
Extract pairs of genes belonging to the same TAG
"""

import argparse
import io


def read_tag_genes(tags_file: io.TextIOWrapper, tag_definitions: list[int]) -> dict:
    """Read the TAGs file and extract the list of genes for each a definition and TAG identifier"""
    tag_genes: dict = {}  # { definition: { number: [ gene identifiers  ] } }
    # Get the columns names index from the header
    column_index = {
        name: index for index, name in enumerate(tags_file.readline().strip().split())
    }
    formatted_tag_definitions = [f"tag{definition}" for definition in tag_definitions]
    # Ensure all requested TAG definitions are present in the file.
    assert all(
        [
            formatted_definition in column_index
            for formatted_definition in formatted_tag_definitions
        ]
    ), 'Not all the given TAGs definitions are present in the columns header names formatted as f"tag{i}" for definition i'
    for row in tags_file:
        parts = row.split("\t")
        gene = parts[column_index["geneName"]]
        for definition in formatted_tag_definitions:
            identifier = parts[column_index[definition]].strip()
            if identifier != "-":
                if definition not in tag_genes:
                    tag_genes[definition] = {}
                if identifier not in tag_genes[definition]:
                    tag_genes[definition][identifier] = [gene]
                else:
                    tag_genes[definition][identifier].append(gene)
    return tag_genes


def write_all_pairs(tag_genes: dict, output_file: io.TextIOWrapper) -> None:
    """Build all pairs and write them to the file along the way

    Parameters
    ----------
        tag_genes: { tag definition: { tag identifier: [list of genes identifiers] } }
    """

    # Write a file header.
    output_file.write(
        "\t".join(["gene_a", "gene_b", "TAG_definition", "TAG_identifier"]) + "\n"
    )
    for definition in tag_genes:
        for tag_identifier in tag_genes[definition]:
            n: int = len(tag_genes[definition][tag_identifier])
            for i in range(n - 1):
                gene_a = tag_genes[definition][tag_identifier][i]
                for j in range(i + 1, n):
                    gene_b = tag_genes[definition][tag_identifier][j]
                    output_file.write(
                        "\t".join([gene_a, gene_b, definition, tag_identifier]) + "\n"
                    )


def main():
    parser = argparse.ArgumentParser(
        description="Extract pairs of genes of the same TAG from the TAGs tsv"
    )
    parser.add_argument(
        "-i",
        "--input",
        type=str,
        help="TAG list file. For some tip on how I expect this TAG file to be formatted, cf. detect_TAGs.py. \
    (Note that I expect this file to contain a header, with column names)",
        required=True,
    )
    parser.add_argument(
        "-o",
        "--output",
        type=str,
        help="Output three columns TSV filename that will contain pairs of genes (in the first two columns) belonging to the same TAG and the associated TAG definition.",
        required=True,
    )
    parser.add_argument(
        "--definitions",
        type=str,
        help="Comma seperated integers for TAG definition (e.g '0,1,5,10' for instance).",
        required=True,
    )
    args = parser.parse_args()
    tags_definitions: str = args.definitions
    tags_definitions: list[int] = list(map(int, tags_definitions.split(",")))
    with open(args.input, "r") as tags_file:
        tag_genes = read_tag_genes(tags_file, tags_definitions)
    with open(args.output, "w") as output_file:
        write_all_pairs(tag_genes, output_file)


if __name__ == "__main__":
    main()
