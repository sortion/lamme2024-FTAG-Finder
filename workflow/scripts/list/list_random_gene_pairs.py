#!/usr/bin/env python3
"""
Extract random pairs of genes from a list of genes
"""

import argparse
import random


parser = argparse.ArgumentParser(
    description="Extract random pairs of genes from a list of genes"
)
parser.add_argument(
    "-i",
    "--input",
    type=str,
    help="Input file with the list of genes",
    required=True,
)
parser.add_argument(
    "-o",
    "--output",
    help="Output file with the random pairs of genes",
    type=str,
    required=True,
)
parser.add_argument(
    "-n",
    "--number",
    type=int,
    help="Number of random pairs to extract",
    required=True,
)
args = parser.parse_args()

with open(args.input, "r") as input_file:
    genes = input_file.read().splitlines()
with open(args.output, "w") as output_file:
    for i in range(args.number):
        gene_a, gene_b = random.sample(genes, 2)
        output_file.write("\t".join([gene_a, gene_b]) + "\n")
