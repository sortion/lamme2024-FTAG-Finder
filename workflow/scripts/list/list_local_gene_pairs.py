#!/usr/bin/env python3
"""
Successive gene pairs and local pairs

Extract all pairs of genes from whatever the family they belong to
provided they are located on the same chromosome seperated by exactly n spacer (gene, or other selected genomic feature present in the features table)
"""

import argparse

from list_successive_gene_pairs import extract_successive_pairs


def read_mapping(mapping_file: str, header: bool = True) -> dict[str, str]:
    """
    Read a two columns file into a dictionnary { gene: family }
    Assumes the first column of the file contains unique keys

    Parameters
    ----------
        mapping_file: the filename of a TSV file with key\tvalue columns
        header: whether to skip the first line

    Outputs
    -------
        a dictionnary { key : value } for each row key\tvalue
    """
    mapping = {}

    with open(mapping_file, "r") as f:
        if header:
            f.readline()  # Skip header
        for line in f:
            parts = line.rstrip().split("\t")
            if len(parts) == 2:
                (key, value) = parts
                mapping[key] = value
    return mapping


def main():
    parser = argparse.ArgumentParser(
        "Extract successive pairs, belonging to the same family"
    )
    parser.add_argument(
        "-i",
        "--input",
        type=str,
        help="Input file with the gene / features positions, with the following columns (name, chromosome, strand, start, stop, type)",
        required=True,
    )
    parser.add_argument(
        "-f",
        "--families",
        type=str,
        help="Genes to families mapping file",
        required=True,
    )
    parser.add_argument(
        "-o",
        "--output",
        type=str,
        help="Output file with the pairs of successive genes",
        required=True,
    )
    parser.add_argument(
        "--definitions",
        type=str,
        help="Comma-seperated list of number of spacers between the genes to consider, example: 0,1,5,10",
        required=True,
    )

    args = parser.parse_args()

    column_names = ["name", "chromosome", "strand", "start", "stop", "type"]
    column_to_index = {
        column_name: index for index, column_name in enumerate(column_names)
    }

    definitions = list(map(int, args.definitions.split(",")))

    # I assume the whole set of features is small enough to fit in memory
    with open(args.input, "r") as input_file:
        features = []
        for line in input_file:
            features.append(line.strip().split("\t"))
            assert len(features[-1]) == len(
                column_names
            ), f"Error: {line} does not contain the expected number of columns"

    # Sort the features by chromosome and start position
    features.sort(
        key=lambda x: (
            x[column_to_index["chromosome"]],
            int(x[column_to_index["start"]]),
        )
    )

    family_dict = read_mapping(args.families)

    with open(args.output, "w") as output_file:
        output_file.write("gene_a\tgene_b\tdefinition\n")
        for n in definitions:
            for gene_a, gene_b in extract_successive_pairs(
                features, n, column_to_index, same_family=True, family_dict=family_dict
            ):
                output_file.write("\t".join([gene_a, gene_b, f"LP{n}"]) + "\n")


if __name__ == "__main__":
    main()
