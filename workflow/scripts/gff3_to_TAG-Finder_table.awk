#!/usr/bin/env -S awk -f
# Convert a standard GFF 3 file into a custom TSV file to be used in TAG Finder Galaxy step
# Usage: gff3-to-TAG_Finder-table.awk input.gff

# Format (documentation extracted from FTAG Finder Galaxy):
# AT1G01010 chr1 1 3631 5899 miRNA
# AT1G01020 chr1 -1 5928 8737 protein_coding
# AT1G01030 chr1 -1 11649 13714 miRNA
# AT1G01040 chr2 1 23146 31227 snoRNA
# : (1)Gene_ID (2)Chromosome (3)Strand (4)Start (5)Stop (6)Type_gene

BEGIN {
    OFS="\t"
    selected["gene"] = 1
    selected["chromosome"] = 1
    selected["miRNA"] = 1
    selected["ncRNA"] = 1
    selected["lincRNA"] = 1
    selected["snoRNA"] = 1
    selected["snRNA"] = 1
    selected["pseudogene"] = 1
}

/^[^#]/ && selected[$3] {
    chromosome=$1
    info=$9
    split(info, infoarr, ";")
    geneid=infoarr[1]
    gsub("ID=", "", geneid)
    gsub("gene:", "", geneid) # in Ensembl GFF.
    orientation=$7
    if (orientation == "+")
        strand="+1"
    else
        strand="-1"
    start=$4
    end=$5
    type=$3
    print geneid, chromosome, strand, start, end, type
}
