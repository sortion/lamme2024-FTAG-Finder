#!/usr/bin/env python3
"""
Gather list of TAGs with various definition

Output:
    A TSV file with the following columns:
    - geneName: gene name
    - chromosome: chromosome name
    - strand: gene strand
    - family: gene family
    - tag0: identifier of the TAGs with definition 0 (0 spacer between two genes of the same family for the gene)
    - tagi:
    - tagn: identifier of the TAGs with definition n (n spacer between two genes of the same family for the gene)
"""
__author__ = "Bérengère Bouillon"

import sys
import logging
import argparse


from reads import readFam, readChromosome, readMap, parser_gff, selection_RNA

logger = logging.getLogger()


def is_same_family(current_family: str, dict_family: dict, next_name: str) -> bool:
    """
    Check if the next gene on the chromosome belongs to the same family of the actual studied gene.
    :param current_family: current family of the actual gene
    :param dict_family: dictionary {geneIDA: family identifier}
    :param next_name: next gene name
    """
    if next_name not in dict_family:
        return False
    elif dict_family[next_name] == current_family:
        return True
    else:
        return False


def rename_mappings(dict_map: dict, dict_family: dict) -> dict:
    """
    Replace gene name key in dict_family with a name from the mapping_file

    Params
    ------
       dict_map: a mapping from previous gene ID to new gene ID { geneID_1 : geneID_2 }
       dict_family: the current mapping from one gene ID to its family { gene_ID_1: family ID }

    Returns
    -------
        dict_family, with gene ID converted { gene ID: family ID }
    """
    for key in dict_family.keys():
        if key in dict_map.keys():
            feature_gene_id = dict_map[key]
            # replace the family geneID by the chromosome geneID
            dict_family[feature_gene_id] = dict_family[key]
    return dict_family


def retrieve_chromosome_list(features: list[list]) -> list[str]:
    """
    Retrieve chromosome list from features file
    """
    chromosomes: list = []
    for feature in features:
        chromosome = feature[1]
        if chromosome not in chromosomes:
            chromosomes.append(chromosome)
    return chromosomes


def TAG_in_all_chromosome(
    features: list[list], TAG_definitions: list[int], family_dict: dict
) -> list[list]:
    """
    Determine TAGs from the list of features for all chromosomes

    Params
    ------
        features:
            List of features, each feature being a list of [feature_name, chromosome, strand, start, stop]
        TAG_definitions:
            List of TAG definitions, definition $n$ means a maximum of $n$ spacer is allowed between two genes of the same TAG.
        family_dict:
            Mapping of a gene name to its family identifier
    Output
    ------
        List of TAGs with the following columns:
        - geneName: gene name
        - chromosome: chromosome name
        - strand: gene strand
        - family: gene family
        - tagi, for i in TAG_definitions: identifier of the TAGs with definition i, or '-' if no TAG respects this definition.
        The idenfifier is unique within a definition and within each chromosome (be aware of this).
    """
    # initialize a list of the different chromosomes passed in the file
    chromosomes: list[str] = []
    # initialize list for genes belonging to a family [geneID]
    chromosomes = retrieve_chromosome_list(features)

    family_TAG_list: list = []

    nb_spacers: int = 0

    # Proceed chromosome by chromosome.
    TAG_number = {definition: 0 for definition in TAG_definitions}
    for chromosome in chromosomes:
        # initialize dictionary for genes belonging to a family {geneID: [strand, chromosome, family, TAG0, TAGn],....}
        # { geneName: { strand, chromosome, family, TAGs: { def: count } } }
        TAGs_dict: dict[str, dict] = {}
        for gene_index, gene_feature in enumerate(features):
            gene_name: str = gene_feature[0]
            gene_chromosome: str = gene_feature[1]
            gene_strand: int = gene_feature[2]
            # If the gene belongs to the current chromosome
            if gene_chromosome == chromosome:
                # if the current gene belongs to a family
                if gene_name in family_dict:
                    family = family_dict[gene_name]  # family being analyzed
                else:
                    family = f"spacers{nb_spacers}"
                    nb_spacers += 1
                # number of genes (not belonging to the same family) between two tags
                nb_inter_TAG = {definition: 0 for definition in TAG_definitions}
                # intialize entry in dictionnary for the gene if it doesn't exist yet
                if gene_name not in TAGs_dict:
                    TAGs_dict[gene_name] = dict(
                        chromosome=gene_chromosome,
                        strand=gene_strand,
                        family=family,
                        TAGs={definition: None for definition in TAG_definitions},
                    )
                for definition in TAG_definitions:
                    # for all compared genes after the index of the reference gene
                    for other_feature in features[gene_index + 1 :]:
                        other_name = other_feature[0]
                        other_chromosome = other_feature[1]
                        other_strand = other_feature[2]
                        if other_chromosome == chromosome:
                            if nb_inter_TAG[definition] <= definition:
                                # if next gene belongs to the same family
                                if is_same_family(family, family_dict, other_name):
                                    nb_inter_TAG[definition] = 0
                                    # If the gene was not considered as belonging to a TAG yet
                                    # Increment the TAG number for the definition
                                    # And set the TAG number for the gene
                                    if TAGs_dict[gene_name]["TAGs"][definition] is None:
                                        TAG_number[definition] += 1
                                        TAGs_dict[gene_name]["TAGs"][definition] = (
                                            TAG_number[definition]
                                        )
                                    if other_name not in TAGs_dict:
                                        # Initialize dictionary entry for the other TAG
                                        # if it wasn't recorded yet
                                        TAGs_dict[other_name] = dict(
                                            chromosome=chromosome,
                                            strand=other_strand,
                                            family=family,
                                            TAGs={
                                                definition: None
                                                for definition in TAG_definitions
                                            },
                                        )
                                        # Set the TAG number for the other gene
                                        TAGs_dict[other_name]["TAGs"][definition] = (
                                            TAG_number[definition]
                                        )
                                    elif (
                                        TAGs_dict[other_name]["TAGs"][definition]
                                        is None
                                    ):
                                        TAGs_dict[other_name]["TAGs"][definition] = (
                                            TAG_number[definition]
                                        )
                                else:
                                    # If the gene does not belong to the same family
                                    # Increment the number of spacers
                                    nb_inter_TAG[definition] += 1
                            else:
                                # The number of spacer is greater than the definition
                                # Reset the number of spacers
                                # Change of TAG
                                nb_inter_TAG[definition] = 0
                                break
        # Put values in a list in order to sort them
        for gene, tag in TAGs_dict.items():
            record = [gene]  # get gene id info
            record.extend(
                [
                    tag["chromosome"],
                    tag["strand"],
                    tag["family"],
                    *[tag["TAGs"][definition] for definition in TAG_definitions],
                ]
            )  # get strand, family end tag infos
            family_TAG_list.append(record)
    return sort_TAG_list_by_position(family_TAG_list, features)


def sort_TAG_list_by_position(TAG_list: list, features: list):
    # sorted list to have genes in the chromosome order in the output file
    TAG_list.sort(key=lambda x: x[0])
    # this code must sort by position and not gene's name, creation of list with name's gene in order on the genome
    order = []
    for feature in features:
        order.append(feature[0])
        sorted_family_TAG_list = [0] * len(order)  # initializatioin of new famTAGList
    for element in TAG_list:
        if element[0] in order:
            indice: int = order.index(element[0])
            # we put in new list elements with tag in order
            sorted_family_TAG_list[indice] = element
    sorted_family_TAG_list = [x for x in sorted_family_TAG_list if x != 0]
    return sorted_family_TAG_list


def task(
    chromosome: str = None,
    families_tsv: str = None,
    mapping_tsv: str = None,
    output_tsv: str = None,
    tag_definitions: str = None,
    chromosome_format: str = None,
    feature_id_is_gene_id: bool = True,
    coding: bool = False,
    use_miRNA: bool = False,
    use_snoRNA: bool = False,
    use_snRNA: bool = False,
    use_lincRNA: bool = False,
    verbose: int = 0,
) -> None:
    """
    Find TAGs (Tandemly Arrayed Genes) in a chromosome.
    """
    log_levels = [logging.ERROR, logging.DEBUG, logging.INFO]
    if verbose < 0 or verbose > len(log_levels):
        raise ValueError(
            f"Invalid verbosity level, must be between 0 and {len(log_levels)}.",
        )
    logging.basicConfig(level=log_levels[verbose])

    if chromosome_format == "gb" or chromosome_format == "txt":
        features: list = readChromosome(
            chromosome
        )  # [[gene, strand, start, stop],[...]] or [[gene, chr, strand, start, stop],[...]]
    elif chromosome_format == "gff":
        features = parser_gff(chromosome)
    else:
        raise ValueError(
            f"{chromosome_format} chromosome file format is not supported."
        )

    family_dict = readFam(families_tsv)

    selected_features = selection_RNA(
        features, coding, use_miRNA, use_snoRNA, use_snRNA, use_lincRNA
    )

    if tag_definitions is None:
        raise ValueError("You should set TAG definition you want to retrieve.")
    TAG_definitions = list(map(int, tag_definitions.split(",")))
    # TAG numerotation on the chromosome initialized to 0 for each TAG definition [0, 0, 0, ...]
    # TAGnumber = [0] * len(TAGdefinition)
    needs_mapping: bool = not feature_id_is_gene_id
    if needs_mapping:
        # mapping dictionary {geneIDA: geneIDB}
        dict_map = readMap(mapping_tsv, header=False)  # dictionary {geneIDA: geneIDB}
        family_dict = rename_mappings(dict_map, family_dict)
    # print(selected_features)

    # means there is the chromosome information, so one file contains several chromosomes
    file_is_valid = len(selected_features[0]) == 5
    assert file_is_valid, "feature file does not have 5 columns"
    if file_is_valid:
        famTAGList = TAG_in_all_chromosome(
            selected_features, TAG_definitions, family_dict
        )
        assert len(famTAGList) > 0, "Families TAG list is empty"

    # Write output
    if output_tsv is not None:
        head = []
        for definition in TAG_definitions:
            head.append(f"tag{definition}")
        if len(features[1]) == 4:
            header = "geneName\tstrand\tfamily\t" + "\t".join(head) + "\n"
        if len(features[1]) == 5:
            header = "geneName\tchromosome\tstrand\tfamily\t" + "\t".join(head) + "\n"
        with open(output_tsv, "w") as out:
            out.write(header)
            for line in famTAGList:
                line = ["-" if x is None else x for x in line]
                line = [str(x) for x in line]
                line = ["+1" if i == 2 and x == "1" else x for i, x in enumerate(line)]
                out.writelines("\t".join(line) + "\n")
    else:
        print(
            "Warning: you did not set any output filename. Not writing results to any file",
            file=sys.stderr,
        )  # would maybe benefit from a logger.


def cli():
    parser = argparse.ArgumentParser(
        description="Detect TAGs (Tandemly Arrayed Genes) from a list of gene / features positions and gene families."
    )
    parser.add_argument(
        "--features",
        type=str,
        help="Path to the chromosome file.",
        required=True,
    )
    parser.add_argument(
        "--features-format",
        type=str,
        help="Chromosome file format.",
        required=True,
    )
    parser.add_argument(
        "--families",
        type=str,
        help="Path to the families TSV file.",
        required=True,
    )
    parser.add_argument(
        "--mapping",
        type=str,
        help="Path to the mapping TSV file.",
        required=False,
        default=None,
    )
    parser.add_argument(
        "-o",
        "--output",
        type=str,
        help="Path to the output TSV file.",
        required=True,
    )
    parser.add_argument(
        "--tag-definitions",
        type=str,
        help="Comma-separated list of TAG definitions.",
        required=True,
    )
    parser.add_argument(
        "--feature-id-is-gene-id",
        action=argparse.BooleanOptionalAction,
        help="Feature ID is gene ID.",
    )
    parser.add_argument(
        "--coding",
        action=argparse.BooleanOptionalAction,
        help="Use coding genes only.",
    )
    parser.add_argument(
        "--use-miRNA",
        action=argparse.BooleanOptionalAction,
        help="Use miRNA.",
        default=False,
    )
    parser.add_argument(
        "--use-snoRNA",
        action=argparse.BooleanOptionalAction,
        help="Use snoRNA.",
        default=False,
    )
    parser.add_argument(
        "--use-snRNA",
        action=argparse.BooleanOptionalAction,
        help="Use snRNA.",
        default=False,
    )
    parser.add_argument(
        "--use-lincRNA",
        action=argparse.BooleanOptionalAction,
        help="Use lincRNA.",
        default=False,
    )

    args = parser.parse_args()

    task(
        chromosome=args.features,
        families_tsv=args.families,
        mapping_tsv=args.mapping,
        output_tsv=args.output,
        tag_definitions=args.tag_definitions,
        chromosome_format=args.features_format,
        feature_id_is_gene_id=args.feature_id_is_gene_id,
        coding=args.coding,
        use_miRNA=args.use_miRNA,
        use_snoRNA=args.use_snoRNA,
        use_snRNA=args.use_snRNA,
        use_lincRNA=args.use_lincRNA,
        verbose=0,
    )


if __name__ == "__main__":
    cli()
