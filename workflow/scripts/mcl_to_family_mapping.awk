#!/usr/bin/env -S awk -f
# MCL output is a tab-separated file where each line contains a list of gene IDs
# We convert this to a two-column file where the first column is the gene ID and the second column is the family index
# Equivalent to legacy Galaxy: Clustering/MCL/treatment_mcl.py
BEGIN {
    FS="\t"
    OFS="\t"
    family_index=1
}
{
    for (idx = 1; idx <= NF; idx++) {
        print $idx, family_index
    }
    family_index++
}
