#!/usr/bin/env python3
"""
FTAG-Finder legacy parsers.

Initially authored by Bérengère Bouillon, Kévin Normand and Séanna Charles


The txt feature file assumes the following columns:
    - gene name
    - chromosome
    - strand
    - start
    - stop
    - type of gene
"""
__author__ = "Bérengère Bouillon"

import re
import sys

from Bio import SeqIO


def readMap(mapFile, header=True):
    """
    This function read the mapping file with column format : protID\tgeneID or geneID\tgeneID
    """
    dictMap = {}
    with open(mapFile, "r") as file1:
        if header:
            file1.readline()
        for line in file1.readlines():
            line = line.strip("\n")
            line = line.split("\t")
            IDA = line[0]
            IDB = line[1]
            if IDA not in dictMap:
                dictMap[IDA] = IDB
    return dictMap


def readFastaPLength(inputFasta):
    """
    This function reads a sequence file in fasta format, and then returns a dictionnary with one entry per sequence (key=ID, value=sequence).
    :param filename: name of the input file supposed to be in fasta format
    :return dictSeq: a list of strings, each corresponding to one sequence in the input file
    """
    protID = ""  # protein id
    dictPLen = {}  # dictionnary {protein: length}
    with open(inputFasta, "r") as file1:
        for line in file1.readlines():
            if line.startswith(">"):
                if " " not in line:
                    protID = line[1:-1]
                else:
                    protID = line[1 : line.index(" ")]
                dictPLen[protID] = 0
            else:
                line = line.strip(" *\n")
                dictPLen[protID] += len(
                    line
                )  # length of the line without the \n at the end

    return dictPLen


def readFastaLength(inputFasta, dictMap):
    """
    This function reads a sequence file in fasta format, and then returns two dictionaries.
    :param inputFasta: name of the input file supposed to be in fasta format
    :return dictPLen: dictionary {protein: length}
    :return dictGPLen: nested dictionary {geneID : {protIDA : length, protIDA : length}}
    """
    protID = ""  # protein id
    dictPLen = {}  # dictionary {protein: length}
    dictGPLen = {}  # nested dictionary {geneID : {protIDA : length, protIDA : length}}

    with open(inputFasta, "r") as file1:
        for line in file1.readlines():
            if line.startswith(">"):
                if " " not in line:
                    protID = line[1:-1]  # id without the ">" and "\n"
                    if protID in dictMap:
                        geneID = dictMap[protID]
                    else:
                        continue
                else:
                    protID = line[1 : line.index(" ")]
                    if protID in dictMap:
                        geneID = dictMap[protID]
                    else:
                        continue

                dictPLen[protID] = 0
                if geneID not in dictGPLen:
                    dictGPLen[geneID] = {}
                if protID not in dictGPLen[geneID]:
                    dictGPLen[geneID][protID] = 0
            else:
                if protID in dictPLen:
                    dictPLen[protID] += len(
                        line[:-1]
                    )  # length of the line without the \n at the end
                if geneID in dictGPLen:
                    if protID in dictGPLen[geneID]:
                        line = line.strip(" *\n")
                        dictGPLen[geneID][protID] += len(
                            line
                        )  # length of the line without the \n at the end

    return dictPLen, dictGPLen


def readFastaGPLength(inputFasta, dictMap):
    """
    This function reads a sequence file in fasta format, and then returns two dictionaries.
    :param filename: name of the input file supposed to be in fasta format
    :return dictPLen: dictionary {protein: length}
    :return dictGPLen: nested dictionary {geneID : {protIDA : length}} # this comment was

    # dictGPLen stands for 'dictionary gene, protein, length'.
    """
    protID = ""  # protein id
    dictGPLen = {}  # nested dictionary {geneID : {protIDA : length}}

    with open(inputFasta, "r") as file1:
        for line in file1.readlines():
            if line.startswith(">"):
                if " " not in line:
                    protID = line[1:-1]  # id without the ">" and "\n"
                    if protID in dictMap:
                        geneID = dictMap[protID]
                    else:
                        continue
                else:
                    protID = line[1 : line.index(" ")]
                    if protID in dictMap:
                        geneID = dictMap[protID]
                    else:
                        continue

                if geneID not in dictGPLen:
                    dictGPLen[geneID] = {}
                if protID not in dictGPLen[geneID]:
                    dictGPLen[geneID][protID] = 0
            else:
                if geneID in dictGPLen:
                    if protID in dictGPLen[geneID]:
                        line = line.strip(" *\n")
                        dictGPLen[geneID][protID] += len(
                            line
                        )  # length of the line without the \n at the end

    return dictGPLen


def readBlastList(inputBlast):
    """
    This function reads a file in -m8 format, and then returns a dictionnary with one entry per sequence (key=ID, value=sequence).
    :param inputBLast:
    columns format: idA\tidB\tsimilitude\tlenAlign\tmismatches\tgap\tbeginA\tendA\tbeginB\tendB\teValue\tbitScore
    :return: array with file values by line
    """
    listBlast = []
    appendList = listBlast.append
    with open(inputBlast, "r") as file1:
        for line in file1.readlines():
            line = line.strip("\n")
            appendList(line)
    return listBlast


def readBlastDict(inputBlast):
    """
    This function reads a file in -m8 format, and then returns a dictionnary with one entry per sequence (key=ID, value=sequence).
    :param inputBLast: results of the Blast with columns format: idA idB similitude lenAlign mismatches gap beginA endA beginB endB eValue bitScore
    :return: hash table with one couple of proteins as key, and list of values for each key
    """
    dictBlast = {}
    with open(inputBlast, "r") as file1:
        for line in file1:
            line = line.split("\t")
            key = (line[0], line[1])
            # values are converted in float to be stored in the dictionary because it consummes less memory
            value = [float(line[i]) for i in range(2, 12)]
            if key not in dictBlast:
                dictBlast[key] = [value]
            else:
                dictBlast[key].append(value)
    return dictBlast


def readBlastDict2(inputBlast):
    """
    This function reads a file in -m8 format, and then returns a dictionnary with one entry per sequence (key=ID, value=sequence).
    :param inputBLast: results of the Blast with columns format: idA idB similitude lenAlign mismatches gap beginA endA beginB endB eValue bitScore
    :return: hash table with one couple of proteins as key, and list of values for each key
    """
    dictBlast = {}
    with open(inputBlast, "r") as file1:
        for line in file1:
            line = line.split("\t")
            key = (line[0], line[1])
            # values are converted in float to be stored in the dictionary because it consummes less memory
            value = [float(line[i]) for i in range(2, 12)]
            if key not in dictBlast:
                dictBlast[key] = [value]
            else:
                dictBlast[key].append(value)
    return dictBlast


def readMergeList(inputMergeC):
    """
    This function reads output file of the merge_blast.c script.
    :param inputMergeC:
    columns format: idA idB nbNonOverlapAlignment similitudeCumul lenAlignACumul lenAlignACumul eValueCumul bitScoreCumul
    :return: array with file values by line
    """
    listMerge = []

    with open(inputMergeC, "r") as f:
        f.readline()
        for line in f:
            listMerge.append(line.strip("\n"))
    return listMerge


def readMergeDict(inputMergeC, skip_header=True):
    """
    This function reads output file of the merge_blast.c script.
    :param inputMergeC:
    columns format: idA idB nbNonOverlapAlignment similitudeCumul lengthCumul lenAlignACumul lenAlignACumul bitScore evalue
    :return: array with file values by line
    """
    dictmerge = {}

    with open(inputMergeC, "r") as f:
        if skip_header:
            f.readline()
        for line in f:
            line = line.strip("\n")
            line = line.split("\t")
            key = (line[0], line[1])
            if len(line) == 9:
                value = [
                    float(line[2]),
                    float(line[3]),
                    float(line[4]),
                    float(line[5]),
                    float(line[6]),
                    float(line[8]),
                    float(line[7]),
                ]
            else:
                value = [
                    float(line[2]),
                    float(line[3]),
                    float(line[11]),
                    float(line[10]),
                    float(line[7]) - float(line[6]),
                    float(line[9]) - float(line[8]),
                ]
            if key not in dictmerge:
                dictmerge[key] = value
            else:
                continue
    return dictmerge


def readPersoList(inputSelection):
    """
    This function reads personal list of proteins ids with one id per row.
    :param inputSelection: txt format
    :return: a list of ids
    """
    selectionDict = {}

    with open(inputSelection, "r") as file1:
        file1.readline()
        for line in file1.readlines():
            line.strip("\n")
            line = line.split("\t")
            if line[0] not in selectionDict:
                selectionDict[line[0]] = None
    return selectionDict


def readHomologs(fileHom):
    """
    This function ...
    """
    dictHom = {}

    with open(fileHom, "r") as f:
        f.readline()
        for line in f.readlines():
            line = line.split("\t")
            nameA = line[0].split(".")
            nameA = nameA[0]
            nameB = line[1].split(".")
            nameB = nameB[0]
            if nameA not in dictHom:
                dictHom[nameA] = ""
            if nameB not in dictHom:
                dictHom[nameB] = ""
    return dictHom


def readFam(famFile):
    """
    This function read a file of gene families formatted like "geneName\tfamilyNumber\n".
    :param famFile: families file
    :return: dictinnary of gene families
    """
    gene_family = {}  # initialyze family file

    # Open family file
    with open(famFile, "r") as f:
        f.readline()
        for line in f:
            try:
                (gene, family) = line.rstrip().split("\t")  # get couple gene/family
                # Fill dictionary gene/family, except for families 0
                if int(family) > 0:
                    gene_family[gene] = int(family)
            except ValueError:
                print("Error in families file on line : " + line)
    return gene_family


def readChromosome(chrFile):
    """
    This function read a chromosome file at Genbank or txt format.
    :param chrFile: chromosome file
    :return: feature table for each gene
    """
    features = []  # initialize features table [name, strand, start, stop]
    fileFormat = ""

    with open(chrFile, "r") as f:
        line1 = f.readline()
        line2 = f.readline()
        if line1.startswith("LOCUS") and line2.startswith("DEFINITION"):
            fileFormat = "gb"
        else:
            fileFormat = "txt"

    if fileFormat == "gb":
        record = SeqIO.parse(chrFile, "genbank")

        # For all CDS of the chromosome, initialyze features
        for feat in record:
            for info in feat.features:  ###ADD_BY CHARLES Seanna### 08/06/2023

                feats = []

                if info.type == "gene":
                    location = list(info.location)
                    start = location[0]
                    stop = location[-1]

                if info.location.strand < 0:  # reverse dialing for complementary strand

                    mem = start
                    start = stop
                    stop = mem
                    print(info)
                    feats.append(
                        info.qualifiers["db_xref"][0].split(":")[1].split("'")[0]
                    )  # feature name
                    feats.append(
                        info.location.strand
                    )  # feature strand direction ; 1 = plus strand, -1 = minus strand
                    feats.append(start)
                    feats.append(stop)
                    feats.append("protein_coding")
                    features.append(feats)

                if info.type == "ncRNA":
                    if "ncRNA_class" in info.qualifiers:
                        location = list(info.location)
                        start = location[0]
                        stop = location[-1]
                        if (
                            info.location.strand < 0
                        ):  # reverse dialing for complementary strand

                            mem = start
                            start = stop
                            stop = mem
                            feats.append(
                                info.qualifiers["db_xref"][0]
                                .split(":")[1]
                                .split("'")[0]
                            )
                            feats.append(info.location.strand)
                            feats.append(start)
                            feats.append(stop)
                            gene_type = info.qualifiers["ncRNA_class"][0].lower()
                            feats.append(gene_type)
                            features.append(feats)

    if fileFormat == "txt":  # gene, chromosome, strand, start, stop
        with open(chrFile, "r") as f:
            f.readline()
            for line in f:
                feats = []
                line = line.rstrip(" \n")
                line = re.split(r"[\t\|]", line)
                feats.append(line[0])  # name
                feats.append(line[1])  # chromosome
                feats.append(line[2])  # strand
                feats.append(int(line[3]))  # start
                feats.append(int(line[4]))  # stop
                feats.append(line[5])  # type of gene
                features.append(feats)
            features.sort(key=lambda x: (x[1], int(x[3])))

    return features


def parser_gff(fichier_gff):  # parser for gff3
    search_biotype = re.compile(r".+?biotype.*?")  # research the word biotype
    search_note = re.compile(r".+?Note.*?")  # research the word Note
    search_debut = re.compile(r"#.*?")

    fichier = open(fichier_gff, "r")
    resultat = []
    liste_gene = []
    for ligne in fichier:
        if not re.search(search_debut, ligne):
            gene_type = ""
            ligne = ligne.rstrip().split("\t")

            if re.search(search_biotype, ligne[8]):
                gene_type = ligne[8].split("biotype=")[1].split(";")[0]
            if re.search(search_note, ligne[8]):
                gene_type = ligne[8].split(";")[1].split("=")[1]

            inter = []
            if gene_type != "":
                inter.append(recherche_id(ligne[8]))
                inter.append(ligne[0])
                inter.append(ligne[6])
                inter.append(int(ligne[3]))
                inter.append(int(ligne[4]))
                inter.append(gene_type)
                if inter[0] not in liste_gene:
                    resultat.append(inter)
                    liste_gene.append(inter[0])
    fichier.close()
    return resultat


def recherche_id(partie_ligne):
    search_id_gene = re.compile(r".+?gene.*?")
    search_name_gene = re.compile(r".+?Name.*?")
    if re.search(search_id_gene, partie_ligne):
        return partie_ligne.split("gene:")[1].split(";")[0]
    if re.search(search_name_gene, partie_ligne):
        return partie_ligne.split("Name=")[1].split(";")[0]


def recherche_info(partie_ligne):
    return partie_ligne.split(";")[1].split("=")[1]


def recherche_version(partie_ligne):
    return partie_ligne.split(";")[0].split("=")[1]


def selection_RNA(
    features, option_coding, option_miRNA, option_snoRNA, option_snRNA, option_lincRNA
):
    tab_option = []
    new_features = []
    if option_coding:
        tab_option.append("protein_coding_gene")
        tab_option.append("protein_coding")
        tab_option.append("gene")
    if option_miRNA:
        tab_option.append("miRNA")
    if option_snoRNA:
        tab_option.append("snoRNA")
    if option_snRNA:
        tab_option.append("snRNA")
    if option_lincRNA:
        tab_option.append("lincRNA")
    for feat in features:
        if feat[-1] in tab_option:
            del feat[-1]
            new_features.append(feat)
    new_features = sorted(new_features, key=lambda position: position[3])
    new_features = sorted(new_features, key=lambda chromosome: chromosome[1])
    return new_features
