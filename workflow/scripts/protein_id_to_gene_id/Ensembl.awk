#!/usr/bin/env -S awk -f
# Extract protein ID and its parent gene ID from a protein FASTA file coming from FlyBase with a fasta record header like:
# >ENSCCEP00000000007.1 pep primary_assembly:cyaCae2:PDCF01000016.1:19181472:19184455:1 gene:ENSCCEG00000000007.1 transcript:ENSCCET00000000012.1 gene_biotype:protein_coding transcript_biotype:protein_coding
# I will output:
# ENSCCEP00000000007.1  ENSCCEG00000000007.1
#
# Usage:
# zcat Danio_rerio.GRCz11.pep.all.fa.gz | awk -f Ensembl.awk

BEGIN {
    OFS="\t"
}

/^>/ {
    protein_id = $1
    sub(/^>/, "", protein_id)
    gene_id = $4
    sub(/gene:/, "", gene_id)
    print protein_id, gene_id
}
