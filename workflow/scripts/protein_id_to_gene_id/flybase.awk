#!/usr/bin/env -S awk -f
# Extract protein ID and its parent gene ID from a protein FASTA file coming from FlyBase with a fasta record header like:
# >FBpp0070000 type=polypeptide; loc=X:join(19963955..19964071,19964782..19964944,19965006..19965126,19965197..19965511,19965577..19966071,19966183..19967012,19967081..19967223,19967284..19967460); ID=FBpp0070000; name=Nep3-PA; parent=FBgn0031081,FBtr0070000; dbxref=FlyBase:FBpp0070000,FlyBase_Annotation_IDs:CG9565-PA,GB_protein:AAF45370.2,REFSEQ:NP_523417,GB_protein:AAF45370,UniProt/Swiss-Prot:Q9W5Y0; MD5=19dfee3c4d8ec74f121a5b5f7f7682e1; length=786; release=r6.57; species=Dmel;
#
# Usage:
# zcat dmel-all-translation-r6.57.fasta.gz | awk -f flybase.awk
#
BEGIN {
    OFS="\t"
}

/^>/ {
    protein_id=$1
    gsub(/^>/, "", protein_id)
    record_name=$0
    split(record_name, a, "; ")
    parent=a[5]
    gsub(/parent=/, "", parent)
    split(parent, parrent_arr, ",")
    parent_gene_id=parrent_arr[1]
    # Assert parent_gene_id starts with FBgn
    if (parent_gene_id !~ /^FBgn/) {
        print "Error: parent_gene_id does not start with FBgn: " parent_gene_id > "/dev/stderr"
        print "Not sure how to handle this case. Exiting." > "/dev/stderr"
        exit 1
    }
    print protein_id, parent_gene_id
}
