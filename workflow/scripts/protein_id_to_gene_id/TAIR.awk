#!/usr/bin/env -S awk -f
# Extract protein ID and gene ID mapping from a FASTA file
# Specifically adapted to TAIR - like pep. FASTA header format:
# ><isoformid>
# where <isoformid> is formmatted as <geneid>.<isoform number>
# Example:
# >AT1G51370.2 | Symbols:  | F-box/RNI-like/FBD-like domains-containing protein | chr1:19045615-19046748 FORWARD LENGTH=346
# I will simply extract:
# AT1G51370.2   AT1G51370
# Usage:
# awk -f TAIR.awk TAIR10_pep_20101214.faa

BEGIN {
    OFS="\t"
}

/^>/ {
    isoform=$1;
    gsub(/^>/, "", isoform);
    split(isoform, isoform_arr, "." );
    print isoform, isoform_arr[1];
}
