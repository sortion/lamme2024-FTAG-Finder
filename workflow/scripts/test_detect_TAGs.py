import detect_TAGs


def test_is_same_family():
    family_dict = {"geneA": "family1", "geneB": "family2", "geneC": "family1"}

    assert detect_TAGs.is_same_family("family1", family_dict, "geneC")
    assert not detect_TAGs.is_same_family("family1", family_dict, "geneB")
