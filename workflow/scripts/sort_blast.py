#!/usr/bin/env python3
"""
FTAG-Finder sortBlast.py
"""

__author__ = "Bérengère Bouillon"
import argparse

try:
    from reads import readFastaGPLength, readMap, readPersoList
except Exception as e:
    print("Import error in sort_blast.py")
    print(e)
    exit()


def keep_one_isoform(
    selection: str,
    gene_protein_length: dict,
    custom_list_filename: str = None,
    selection_alternative: str = None,
) -> set:
    """
    Keep only one type of isoform.

    Params
    ------
        selection: either "own" or "computed", specifying whether to retrieve the list of isoform from a user defined file
            or to get this list using a computed criterion, defined in `selection_alternative`
        custom_list_filename: a one column filename with isoform ID to keep
        gene_protein_length: a mapping from gene ID to protein isoforms ID to sequence length {geneID: { proteinID: protein_length }}
        selection_alternative: either "shortest" or "longest", when using selection="computed"

    Output
    ------
        a set of isoform ID to keep
    """
    kept_isoforms: set = set()

    # Validate function arguments

    selection_valid_values = ["own", "computed"]
    if selection not in selection_valid_values:
        raise ValueError(f"selection not in {selection_valid_values}")

    if selection == "computed":
        selection_alternative_valid_values = ["shortest", "longest"]
        if selection_alternative not in selection_alternative_valid_values:
            raise ValueError(
                f"selection_alternative not in {selection_alternative_valid_values}"
            )

    # Keep proteins from user defined selection
    if selection == "own":
        input_selection = readPersoList(custom_list_filename)  # {protID: None}
        for gene_key, isoforms in gene_protein_length.items():
            for protein_key in isoforms.keys():
                if protein_key not in input_selection.keys():
                    del gene_protein_length[gene_key][protein_key]

    elif selection == "computed":
        # Keeps the longest isoform
        if selection_alternative == "longest":
            for gene_key, isoforms in gene_protein_length.items():
                for protein_key_1 in isoforms.keys():
                    for protein_key_2 in isoforms.keys():
                        if protein_key_1 != protein_key_2:
                            if (
                                gene_protein_length[gene_key][protein_key_1]
                                >= gene_protein_length[gene_key][protein_key_2]
                            ):
                                del gene_protein_length[gene_key][protein_key_2]
                            else:
                                del gene_protein_length[gene_key][protein_key_1]

        ### Keeps the shortest isoform
        if selection_alternative == "shortest":
            for gene_key, isoforms in list(gene_protein_length.items()):
                for protein_key_1 in list(isoforms.keys()):
                    for protein_key_2 in list(isoforms.keys()):
                        if protein_key_1 != protein_key_2:
                            if (
                                gene_protein_length[gene_key][protein_key_1]
                                <= gene_protein_length[gene_key][protein_key_2]
                            ):
                                del gene_protein_length[gene_key][protein_key_2]
                            else:
                                del gene_protein_length[gene_key][protein_key_1]

    for gene_key, value in list(gene_protein_length.items()):
        isoform_id = list(value.keys())[0]
        kept_isoforms.add(isoform_id)

    return kept_isoforms


def read_tsv(filename: str) -> list[list[str]]:
    """
    Read a TSV file and return a list of list of values

    """
    with open(filename, "r") as file:
        lines = file.readlines()
        return [line.strip().split("\t") for line in lines]


def task(
    input_blast: str,
    output_blast: str,
    input_fasta: str,
    filter_isoforms: bool,
    selection: str,
    protein_id_is_gene_id: bool,
    protein_id_to_gene_id: str = None,
    custom_list_filename: str = None,
) -> None:
    """
    Sort BLASTP output

    Params
    ------
        input_blast: input BLASTP filename (TSV format)
        output_blast: filtered output BLASTP filename (TSV format)
        input_fasta: input FASTA filename
        filter_isoforms: whether to filter isoform
        selection: either "own" or "computed", specifying whether to retrieve the list of isoform from a user defined file
            or to get this list using a computed criterion, defined in `selection_alternative`
        protein_id_is_gene_id: whether the protein ID is the same as the gene ID
        protein_id_to_gene_id: a two columns TSV file with the first row "proteinID\tgeneID" (only if protein_id_is_gene_id=True)
        custom_list_filename: a one column filename with isoform ID to keep (only if selection="own")

    Output
    ------
        Filtered BLASTP output
    """
    # idA idB similitude lenAlign mismatches gap beginA endA beginB endB eValue bitScore
    blast_records = read_tsv(input_blast)
    if not protein_id_is_gene_id:
        protein_gene_mapping = readMap(
            protein_id_to_gene_id
        )  # dictionnary {protein: gene}
    blast_records_filtered = []

    # Filter by isoform (optionnal)
    if protein_id_is_gene_id and filter_isoforms:
        gene_protein_length = readFastaGPLength(input_fasta, protein_gene_mapping)
        # dictGPLen : nested dictionary {geneID: {protIDA: length, protIDB: length}}
        kept_isoforms = keep_one_isoform(
            selection,
            custom_list_filename=custom_list_filename,
            gene_protein_length=gene_protein_length,
        )  # {protID: None}
        for line in blast_records:
            id_A = line[0]
            id_B = line[1]
            if id_A in kept_isoforms and id_B in kept_isoforms:
                blast_records_filtered.append(line)
        blast_records = blast_records_filtered

    # Add gene IDs in order to sort values by gene IDs
    blast_records_with_gene_IDs = []
    if protein_id_is_gene_id:
        for line in blast_records:
            protein_id_A = line[0]
            protein_id_B = line[1]
            if (
                protein_id_A in protein_gene_mapping
                and protein_id_B in protein_gene_mapping
            ):
                gene_id_A = protein_gene_mapping[protein_id_A]
                gene_id_B = protein_gene_mapping[protein_id_B]
                blast_record_with_gene_IDs = line + [gene_id_A, gene_id_B]
                blast_records_with_gene_IDs.append(blast_record_with_gene_IDs)
        ### Sort results by couple of geneIDs and score
        blast_records_filtered.sort(
            key=lambda x: (
                x[-1],
                x[-2],
                -float(x[11]),
            )
        )
        blast_records = blast_records_filtered

    else:
        # Sort results by couple of geneIDs and score
        blast_records.sort(
            key=lambda x: (
                x[0],
                x[1],
                -float(x[11]),
            )
        )
    write_output(output_blast, blast_records)


def write_output(output_blast: str, blast_records: list[list[str]]) -> None:
    """
    Write the output to a file

    Params
    ------
        output_blast: output BLASTP filename
        blast_records: list of BLASTP records
    """
    with open(output_blast, "w") as outFile:
        outFile.write(
            "nameA\tnameB\tsimil\tlgAlign\tnbMismtch\tnbGap\tbeginA\tendA\tbeginB\tendB\teValue\tbitScore\n"
        )
        for line in blast_records:
            outFile.write("\t".join(line) + "\n")


def main():
    """
    This script filters BLAST results by an isoform type if selected, and then sort results according to their gene ID couple and e-value.
    """
    # Define command line interface
    # TODO:maybe improve command line argument naming.
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", help="Input BLASTP filename")
    parser.add_argument("-o", "--output", help="Output BLASTP filename")
    parser.add_argument(
        "--input-fasta",
        help='Input FASTA file, on which the blastp "all against all" was run',
    )
    parser.add_argument(
        "--protein-id-is-gene-id", action=argparse.BooleanOptionalAction
    )
    parser.add_argument(
        "--protein-id-to-gene-id",
        help='A two columns TSV file with the first row "proteinID\tgeneID"',
        default=None,
    )
    parser.add_argument(
        "--filter-isoform",
        action=argparse.BooleanOptionalAction,
    )
    parser.add_argument(
        "--filter-selection",
        help="Isoform selection method",
        choices=["own", "computed"],
        default="computed",
    )
    parser.add_argument(
        "--personal-selection",
        help='If --filter-selection="own", then provides a filename from which to read the selected isoforms',
        default=None,
    )
    parser.add_argument(
        "--isoform-alternative",
        help='If --filter-selection="computed", provide the selected isoform length alternative',
        choices=["longest", "shortest"],
        default="longest",
    )

    args = parser.parse_args()

    task(
        input_blast=args.input,
        output_blast=args.output,
        input_fasta=args.input_fasta,
        filter_isoforms=args.filter_isoform,
        selection=args.isoform_alternative,
        protein_id_is_gene_id=args.protein_id_is_gene_id,
        protein_id_to_gene_id=args.protein_id_to_gene_id,
        custom_list_filename=args.personal_selection,
    )


if __name__ == "__main__":
    main()
