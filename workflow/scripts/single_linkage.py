"""
Simple single linkage graph clustering computation

Takes simply an edge list TSV (either weighted (.abc), or unweighted, only the first two columns are required and taken into account, corresponding to directly conntected vertex identifiers.

refactored from legacy single_linkage2.py file
"""


def single_linkage(edges_filename, cluster_filename):

    with open(edges_filename, "r") as input_file:
        vertex2cluster = {}
        clusters: dict = {}
        count: int = 0

        for line in input_file:
            if line == "":
                continue
            splitted_line = line.split("\t")
            vertex_1 = splitted_line[0]
            vertex_2 = splitted_line[1]
            # Create new clusters, if neither vertex belong to an existing cluster
            if vertex_1 not in vertex2cluster and vertex_2 not in vertex2cluster:
                count += 1
                vertex2cluster[vertex_1] = count
                vertex2cluster[vertex_2] = count
                clusters[count] = []
                clusters[count].append(vertex_1)
                clusters[count].append(vertex_2)
            # Add the orphan vertex to the cluster of its neighbor with a known cluster
            elif vertex_1 in vertex2cluster and vertex_2 not in vertex2cluster:
                vertex2cluster[vertex_2] = vertex2cluster[vertex_1]
                clusters[vertex2cluster[vertex_1]].append(vertex_2)
            elif vertex_1 not in vertex2cluster and vertex_2 in vertex2cluster:
                vertex2cluster[vertex_1] = vertex2cluster[vertex_2]
                clusters[vertex2cluster[vertex_2]].append(vertex_1)
            # Merge the clusters
            elif vertex_1 in vertex2cluster and vertex_2 in vertex2cluster:
                if vertex2cluster[vertex_1] != vertex2cluster[vertex_2]:  # Avoid loop.
                    cluster_vertex_1 = vertex2cluster[vertex_1]
                    cluster_vertex_2 = vertex2cluster[vertex_2]
                    for i in clusters[cluster_vertex_2]:
                        vertex2cluster[i] = cluster_vertex_1
                    clusters[cluster_vertex_1].extend(clusters[cluster_vertex_2])
                    del clusters[cluster_vertex_2]

    with open(cluster_filename, "w") as output_file:
        output_file.write("geneName\tfamily\n")
        for key in clusters:
            for value in clusters[key]:
                output_file.write("\t".join([str(value), str(key)]))
                output_file.write("\n")
