import single_linkage

abc_filename = snakemake.input["abc_file"]
cluster_filename = snakemake.output["cluster_file"]

single_linkage.single_linkage(abc_filename, cluster_filename)
