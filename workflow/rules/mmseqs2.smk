"""
All against all protein local alignment with MMSeqs2
"""

if alignment_method == "mmseqs2":

    rule mmseqs2_createdb:
        input:
            pep_fasta=input_pep_fasta,
        output:
            db_files=multiext(
                "tmp/mmseqs2/db/{name}",
                "_h",
                "_h.dbtype",
                "_h.index",
                ".index",
                ".lookup",
                ".source",
            ),
        params:
            db=lambda wilcards: f"tmp/mmseqs2/db/{wilcards.name}",
        conda:
            "../envs/mmseqs2.yaml"
        log:
            stderr="logs/mmseqs2_createdb/{name}.stderr",
            stdout="logs/mmseqs2_createdb/{name}.stdout",
        shell:
            """
            mmseqs createdb "{input.pep_fasta}" "{params.db}"
            """

    rule mmseqs2_search:
        input:
            db_files=multiext(
                "tmp/mmseqs2/db/{name}",
                "_h",
                "_h.dbtype",
                "_h.index",
                ".index",
                ".lookup",
                ".source",
            ),
        output:
            blast_tsv="results/mmseqs2/{name}.blastp.tsv",
        params:
            db=lambda wilcards: f"tmp/mmseqs2/db/{wilcards.name}",
            out_db=lambda wilcards: f"tmp/mmseqs2/out/{wilcards.name}",
            tmpdir=lambda wildcards: f"tmp/mmseqs2/tmp/{wildcards.name}",
        threads: 4
        conda:
            "../envs/mmseqs2.yaml"
        log:
            stderr="logs/mmseqs2_search/{name}.stderr",
            stdout="logs/mmseqs2_search/{name}.stdout",
        shell:
            """
            mkdir -p $(dirname "{params.out_db}")
            mkdir -p "{params.tmpdir}"
            mmseqs search "{params.db}" "{params.db}" "{params.out_db}" "{params.tmpdir}" --search-type 2 --threads "{threads}" > "{log.stdout}" 2> "{log.stderr}"
            mmseqs convertalis "{params.db}" "{params.db}" "{params.out_db}" "{output.blast_tsv}" --format-mode 2 --threads "{threads}" >> "{log.stdout}" 2>> "{log.stderr}"
            """
