"""
Blast sort, filter and merge

Takes blastp output
Returns a so-called "pairs" file with columns:
    - queryid
    - subjectid
    - number of overlapping alignments
    - cumulative similarity score
    - cumulative length
    - cumulative length A
    - cumulative length B
    - best E-value
    - best bitscore
"""

custom_list_filename = None
if (
    config["sort_blast_hsp"]["filter_isoform"]
    and config["sort_blast_hsp"]["filter_selection"] == "own"
):
    if "custom_list_filename" in config["sort_blast_hsp"]:
        custom_list_filename = config["sort_blast_hsp"]["custom_list_filename"]
    else:
        ValueError(
            'In config, sort_blast_hsp.custom_list_filenampe have to be set when sort_blast_hsp.filter_selection is set to "own"'
        )


rule sort_blast_hsp:
    """Sort and filter BLAST 6 TSV

    Filter:
        Selection:
        - own: Keep a selection of protein
        - computed:
            - longest
            - shortest
    """
    input:
        input_fasta=input_pep_fasta,
        isoform_protein_mapping=(
            protein_id_to_gene_id_mapping_file
            if protein_id_to_gene_id_mapping_file
            else []
        ),
        blastp_tsv=f"results/{alignment_method}/" + "{name}.blastp.tsv",
        custom_list_filename=custom_list_filename if custom_list_filename else [],
    output:
        blastp_tsv="results/{name}.blastp.sorted.tsv",
    params:
        filter_isoform_option=(
            "--filter-isoform"
            if config["sort_blast_hsp"]["filter_isoform"]
            else " --no-filter-isoform"
        ),
        filter_selection_option=(
            "--filter-selection " + config["sort_blast_hsp"]["filter_selection"]
            if config["sort_blast_hsp"]["filter_isoform"]
            else ""
        ),
        # "own" or "computed"
        protein_id_is_gene_id_option=(
            "--protein-id-is-gene-id"
            if config["protein_id_is_gene_id"]
            else " --no-protein-id-is-gene-id"
        ),
        protein_id_to_gene_id_option=(
            "--protein-id-to-gene-id " + protein_id_to_gene_id_mapping_file
            if not config["protein_id_is_gene_id"]
            else ""
        ),
        isoform_alternative_option=(
            "--isoform-alternative " + config["sort_blast_hsp"]["isoform_alternative"]
            if config["sort_blast_hsp"]["filter_selection"] == "computed"
            else ""
        ),
        # "longest" of "shortest"
        custom_list_filename_option=(
            "--personal-selection " + custom_list_filename
            if custom_list_filename
            else ""
        ),
        script=f"{workflow.current_basedir}/../scripts/sort_blast.py",
    conda:
        "../envs/biopython.yaml"
    log:
        stdout="logs/sort_blast_hsp/{name}.stdout",
        stderr="logs/sort_blast_hsp/{name}.stderr",
    threads: 1
    shell:
        """python3 "{params.script}" \
        --input "{input.blastp_tsv}" \
        --output "{output.blastp_tsv}" \
        --input-fasta "{input.input_fasta}" \
        {params.protein_id_is_gene_id_option} \
        {params.protein_id_to_gene_id_option} \
        {params.filter_isoform_option} \
        {params.filter_selection_option} \
        {params.isoform_alternative_option} \
        {params.custom_list_filename_option} > "{log.stdout}" 2> "{log.stderr}"
        """


if config["merge_blast_hsp"]["merge"]:
    filtered_blastp_tsv = f"results/{run_name}.pairs.tsv"
else:
    filtered_blastp_tsv = f"results/{run_name}.blastp.sorted.tsv"


rule merge_blast_hsp:
    input:
        blastp_tsv="results/{name}.blastp.sorted.tsv",
    output:
        pairs_tsv="results/{name}.pairs.tsv",
        matchs_tsv="results/{name}.matchs.tsv",
    params:
        merge_amino_acid_allowed_overlap=config["merge_blast_hsp"][
            "merge_amino_acid_allowed_overlap"
        ],
    conda:
        "../envs/mergeBlast.yaml"
    log:
        stdout="logs/mergeBlast/{name}.stdout",
        stderr="logs/mergeBlast/{name}.stderr",
    threads: 1
    shell:
        """
        mergeBlast "{input.blastp_tsv}" "{params.merge_amino_acid_allowed_overlap}" "{output.matchs_tsv}" "{output.pairs_tsv}" \
            -auto
        """
