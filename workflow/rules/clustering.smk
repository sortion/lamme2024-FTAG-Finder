

rule mcl:
    input:
        abc="results/{name}.abc",
    output:
        mcl="results/{name}.mcl",
        abc_no_header=temp("results/{name}_noheader.mcl"),
    params:
        # pre_inflation_max_bound=config["mcl"]["pre_inflation_max_bound"],
        inflation=config["mcl"]["inflation"],
        pre_inflation=config["mcl"]["pre_inflation"],
        start_inflation=config["mcl"]["start_inflation"],
        discard_loops="y" if config["mcl"]["remove_loops"] else "n",
    log:
        stderr="logs/mcl/{name}.stderr",
        stdout="logs/mcl/{name}.stdout",
    conda:
        "../envs/mcl.yaml"
    threads: 1
    shell:
        """
        tail -n+2 "{input.abc}" > "{output.abc_no_header}"
        mcl  "{output.abc_no_header}" -te "{threads}" --abc \
            -I "{params.inflation}" -pi "{params.pre_inflation}" \
            -if "{params.start_inflation}" \
            --discard-loops="{params.discard_loops}" -o "{output.mcl}" > "{log.stdout}" 2> "{log.stderr}"
        """
        # Warning: option "-ph" present in mcl 14 disapeared in recent version
        # -ph "{params.pre_inflation_max_bound}"


rule mcl_to_family_mapping:
    input:
        mcl="results/{name}.mcl",
    output:
        tsv="results/{name}.mcl.tsv",
    threads: 1
    log:
        stderr="logs/mcl_to_family_mapping/{name}.stderr",
    params:
        script=f"{workflow.current_basedir}/../scripts/mcl_to_family_mapping.awk",
    shell:
        """
        awk -f "{params.script}" "{input.mcl}" > "{output.tsv}" 2> "{log.stderr}"
        """


rule single_linkage:
    input:
        abc_file="results/{name}.abc",
    output:
        cluster_file="results/{name}.sl.tsv",
    threads: 1
    conda:
        "../envs/python.yaml"
    log:
        stdout="logs/single_linkage/{name}.stdout",
        stderr="logs/single_linkage/{name}.stderr",
    script:
        "../scripts/single_linkage.py"


rule walktrap:
    input:
        abc_file="results/{name}.abc",
    output:
        cluster_file="results/{name}.Walktrap.tsv",
    params:
        density=config["walktrap"]["density"],
        seed=config["walktrap"]["seed"],
        script=f"{workflow.current_basedir}/../scripts/Walktrap.R",
    threads: 1
    conda:
        "../envs/walktrap.yaml"
    log:
        stdout="logs/single_linkage/{name}.stdout",
        stderr="logs/single_linkage/{name}.stderr",
    shell:
        """
        Rscript "{params.script}" "{input.abc_file}" "{params.density}" "{output.cluster_file}"
        """
