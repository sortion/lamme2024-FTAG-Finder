if alignment_method == "blastp":

    rule blastp:
        input:
            query=input_pep_fasta,
            db=expand(
                "tmp/blastp/db/{name}{ext}",
                name="{name}",
                ext=[
                    ".phr",
                    ".pin",
                    ".psq",
                ],
            ),
        output:
            tsv="results/blastp/{name}.blastp.tsv",
        params:
            db=lambda wildcards: f"tmp/blastp/db/{wildcards.name}",
            seg=config["blastp"]["seg"],
            evalue=config["blastp"]["evalue"],
            matrix=config["blastp"]["matrix"],
            gapopen=config["blastp"]["gapopen"],
            gapextend=config["blastp"]["gapextend"],
            threshold=config["blastp"]["threshold"],
            xdrop_gap=config["blastp"]["threshold"],
            format="6",
        threads: 1
        conda:
            "../envs/blast.yaml"
        log:
            stderr="logs/blastp/{name}.stdout",
            stdout="logs/blastp/{name}.stderr",
        shell:
            """
            blastp -query "{input.query}" -db "{params.db}" \
                -out "{output.tsv}" -outfmt "{params.format}" \
                -seg "{params.seg}" -evalue "{params.evalue}" \
                -matrix "{params.matrix}" -threshold "{params.threshold}" \
                -gapopen "{params.gapopen}" -gapextend "{params.gapextend}" \
                -xdrop_gap "{params.xdrop_gap}" \
                -num_threads "{threads}" \
                > "{log.stdout}" 2> "{log.stderr}"
            """

    rule makeblastdb_protein:
        input:
            fasta=input_pep_fasta,
        output:
            multiext(
                "tmp/blastp/db/{name}",
                ".phr",
                ".pin",
                ".psq",
            ),
        params:
            db=lambda wildcards: f"tmp/blastp/db/{wildcards.name}",
        conda:
            "../envs/blast.yaml"
        log:
            stderr="logs/makeblastdb_protein/{name}.stderr",
            stdout="logs/makeblastdb_protein/{name}.stdout",
        shell:
            """
            makeblastdb -in "{input.fasta}" -parse_seqids -dbtype prot -out "{params.db}"
            """
