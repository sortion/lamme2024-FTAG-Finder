if alignment_method == "diamond":

    rule diamond_makedb:
        input:
            fasta=input_pep_fasta,
        output:
            db_file="tmp/diamond/{name}.dmnd",
        params:
            db=lambda w: f"tmp/diamond/{w.name}",
        conda:
            "../envs/diamond.yaml"
        log:
            stderr="logs/diamond_makedb/{name}.stderr",
            stdout="logs/diamond_makedb/{name}.stdout",
        threads: 8
        shell:
            """
            diamond makedb --in "{input.fasta}" --db "{params.db}" > "{log.stdout}" 2> "{log.stderr}"
            """

    rule diamond_blastp:
        input:
            db_file="tmp/diamond/{name}.dmnd",
            query=input_pep_fasta,
        output:
            tsv="results/diamond/{name}.blastp.tsv",
        params:
            db=lambda w: f"tmp/diamond/{w.name}",
        log:
            stderr="logs/diamond_blastp/{name}.stderr",
            stdout="logs/diamond_blastp/{name}.stdout",
        conda:
            "../envs/diamond.yaml"
        shell:
            """
            diamond blastp --db "{params.db}" --query "{input.query}" --out "{output.tsv}" --outfmt 6 > "{log.stdout}" 2> "{log.stderr}"
            """
