"""
A Snakemake workflow to create special list of genes
"""


rule gene_list:
    input:
        features=features_file,
    output:
        genes=f"results/{run_name}_genes.list",
    shell:
        r"""
        cut -d\t -f1 "{input.features}" | sort | uniq > "{output.genes}"
        """


rule random_pairs:
    input:
        genes=f"results/{run_name}_genes.list",
    output:
        pairs=f"results/lists/{run_name}/random_gene_pairs.tsv",
    params:
        size=config["list"]["random_pairs"]["size"],
        script=f"{workflow.current_basedir}/../scripts/list/list_random_gene_pairs.py",
    log:
        stdout="logs/random_pairs.stdout",
        stderr="logs/random_pairs.stderr",
    shell:
        """
        python3 "{params.script}" --input "{input.genes}" --output "{output.pairs}" --number {params.size} > "{log.stdout}" 2> "{log.stderr}"
        """


rule intra_family_pairs:
    input:
        families=f"results/{run_name}.{clustering_method}.tsv",
    output:
        pairs=f"results/lists/{run_name}/intra_family_pairs.tsv",
    params:
        script=f"{workflow.current_basedir}/../scripts/list/list_intra_family_gene_pairs.py",
    log:
        stderr=f"logs/intra_family_pairs/{run_name}.stderr",
    shell:
        """
        python3 "{params.script}" --input "{input.families}" --output "{output.pairs}" > "{log.stderr}"
        """


rule intra_tag_pairs:
    input:
        tags=f"results/{run_name}.{clustering_method}.TAGs.tsv",
    output:
        pairs=f"results/lists/{run_name}/intra_tag_pairs.tsv",
    params:
        definitions=",".join(list(map(str, config["detect_TAGs"]["definitions"]))),
        script=f"{workflow.current_basedir}/../scripts/list/list_TAG_gene_pairs.py",
    log:
        stdout=f"logs/intra_tag_pairs/{run_name}.stdout",
        stderr=f"logs/intra_tag_pairs/{run_name}.stderr",
    shell:
        """
        python3 "{params.script}" --input "{input.tags}" --output "{output.pairs}" --definitions="{params.definitions}" > "{log.stdout}" 2> "{log.stderr}"
        """


rule successive_gene_pairs:
    input:
        features=features_file,
    output:
        pairs=f"results/lists/{run_name}/successive_gene_pairs.tsv",
    params:
        definitions=",".join(list(map(str, config["detect_TAGs"]["definitions"]))),
        script=f"{workflow.current_basedir}/../scripts/list/list_successive_gene_pairs.py",
    log:
        stdout="logs/successive_gene_pairs.stdout",
        stderr="logs/successive_gene_pairs.stderr",
    shell:
        """
        python3 "{params.script}" --input "{input.features}" --output "{output.pairs}" --definitions="{params.definitions}" > "{log.stdout}" 2> "{log.stderr}"
        """


rule local_gene_pairs:
    input:
        features=features_file,
        families=f"results/{run_name}.{clustering_method}.tsv",
    output:
        pairs=f"results/lists/{run_name}/local_gene_pairs.tsv",
    params:
        definitions=",".join(list(map(str, config["detect_TAGs"]["definitions"]))),
        script=f"{workflow.current_basedir}/../scripts/list/list_local_gene_pairs.py",
    log:
        stdout="logs/local_gene_pairs.stdout",
        stderr="logs/local_gene_pairs.stderr",
    shell:
        """
        python3 "{params.script}" --input "{input.features}" --families "{input.families}" --output "{output.pairs}" --definitions={params.definitions} > "{log.stdout}" 2> "{log.stderr}"
        """


rule all_gene_pairs:
    input:
        genes=f"results/{run_name}_genes.list",
    output:
        pairs=f"results/lists/{run_name}/all_gene_pairs.tsv",
    params:
        script=f"{workflow.current_basedir}/../scripts/list/list_all_gene_pairs.py",
    shell:
        """
        python3 "{params.script}" --input "{input.genes}" --output "{output.pairs}"
        """
