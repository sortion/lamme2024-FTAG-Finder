"""
Extract a two columns TSV mapping from protein ID to gene ID
from some standard FASTA headers formats.
"""

import os

extract_protein_and_isoform_id = False
if "extract" in config["geneid_proteinid_mapping"]:
    if "format" in config["geneid_proteinid_mapping"]["extract"]:
        fasta_header_format = config["geneid_proteinid_mapping"]["extract"]["format"]
        protein_id_to_gene_id_mapping_file = f"results/{run_name}.iso2gene.tsv"
        extract_protein_and_isoform_id = True
    else:
        raise ValueError("Please specify the format of the FASTA headers.")
elif "file" in config["geneid_proteinid_mapping"]:
    protein_id_to_gene_id_mapping_file = config["geneid_proteinid_mapping"]["file"]
else:
    raise ValueError("Please specify the input FASTA file.")

script_dir = f"{workflow.current_basedir}/../scripts/protein_id_to_gene_id"

if extract_protein_and_isoform_id:
    if fasta_header_format == "TAIR":
        script = "TAIR.awk"
    elif fasta_header_format == "Ensembl":
        script = "Ensembl.awk"
    elif fasta_header_format == "flybase":
        script = "flybase.awk"
    else:
        raise UnimplementedError(
            f"Unsupported FASTA header format: {fasta_header_format}."
        )

    rule extract_protein_and_isoform_id:
        input:
            input_pep_fasta,
        output:
            protein_id_to_gene_id_mapping_file,
        params:
            script=lambda wildcards: f"{script_dir}/{script}",
        log:
            stderr="logs/extract_protein_and_isoform_id.stderr",
        shell:
            """
            awk -f "{params.script}" "{input}" > "{output}"
            """
