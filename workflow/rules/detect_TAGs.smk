if "extract" in config["features"]:
    features_file = f"results/{run_name}_features.tsv"
elif "file" in config["features"]:
    features_file = config["features"]["file"]
else:
    raise ValueError(
        "No features file defined. Please check the `features` key in the configuration"
    )


rule gff3_to_features_table:
    input:
        input_gff,
    output:
        features_file,
    log:
        stderr="logs/gff3_to_features_table.stderr",
    params:
        script=f"{workflow.current_basedir}/../scripts/gff3_to_TAG-Finder_table.awk",
    shell:
        """
        awk -f "{params.script}" "{input}" > "{output}"
        """


rule detect_TAGs:
    input:
        features=features_file,
        families="results/{name}.{method}.tsv",
    output:
        lists="results/{name}.{method}.TAGs.tsv",
    params:
        tag_definitions=",".join(map(str, config["detect_TAGs"]["definitions"])),
        feature_id_is_gene_id_option=(
            "--feature-id-is-gene-id"
            if config["feature_id_is_gene_id"]
            else " --no-feature-id-is-gene-id "
        ),
        features_format=config["features"]["format"],
        coding=f" --coding " if config["detect_TAGs"]["coding"] else " ",
        using_option=" ".join(
            [
                f"--use-{key}"
                for key in ["miRNA", "snoRNA", "snRNA", "lincRNA"]
                if key in config["detect_TAGs"]["use"]
            ]
        ),
        script=f"{workflow.current_basedir}/../scripts/detect_TAGs.py",
    log:
        stdout="logs/detect_TAGs/{name}.{method}.stdout",
        stderr="logs/detect_TAGs/{name}.{method}.stderr",
    threads: 1
    conda:
        "../envs/biopython.yaml"
    shell:
        """python3 "{params.script}" \
        --features "{input.features}" \
        --families "{input.families}" \
        --tag-definitions "{params.tag_definitions}" \
        --output "{output.lists}" \
        {params.feature_id_is_gene_id_option} \
        --features-format {params.features_format} \
        {params.coding} \
        {params.using_option}
        """
