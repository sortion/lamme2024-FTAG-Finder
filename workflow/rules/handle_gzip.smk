input_pep_fasta = config["input_pep_fasta"]  # proteome fasta file
input_gff = config["input_gff"]  # gff file
run_name = config["run_name"]


if input_pep_fasta.endswith(".gz"):
    input_pep_fasta = input_pep_fasta.replace(".gz", "")

    rule unzip_proteome:
        input:
            input_pep_fasta + ".gz",
        output:
            temp(input_pep_fasta),
        shell:
            """
            gunzip -c "{input}" > "{output}"
            """


if input_gff.endswith(".gz"):
    input_gff = input_gff.replace(".gz", "")

    rule unzip_gff:
        input:
            input_gff + ".gz",
        output:
            temp(input_gff),
        shell:
            """
            gunzip -c "{input}" > "{output}"
            """
