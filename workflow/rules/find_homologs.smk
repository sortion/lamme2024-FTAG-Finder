"""
Build the similarity graph, with homology links between proteins
"""


rule find_homologs:
    input:
        fasta=input_pep_fasta,
        blastp=filtered_blastp_tsv,
        mapping=(
            protein_id_to_gene_id_mapping_file
            if not config["protein_id_is_gene_id"]
            else []
        ),
    output:
        abc="results/{name}.abc",
    params:
        similarity_threshold=config["find_homologs"]["similarity_threshold"],
        coverage_threshold=config["find_homologs"]["cummulated_coverage_threshold"],
        bitscore_selection=config["find_homologs"]["bitscore_selection"],
        protein_id_is_gene_id_option=(
            "--protein-id-is-gene-id"
            if config["protein_id_is_gene_id"]
            else "--no-protein-id-is-gene-id"
        ),
        script=f"{workflow.current_basedir}/../scripts/find_homologs.py",
    conda:
        "../envs/biopython.yaml"
    log:
        stdout="logs/find_homologs/{name}.stdout",
        stderr="logs/find_homologs/{name}.stderr",
    shell:
        """python3 {params.script} \
            --fasta "{input.fasta}" \
            --pairs "{input.blastp}" \
            --output "{output.abc}" \
            --similarity-threshold {params.similarity_threshold} \
            --coverage-threshold {params.coverage_threshold} \
            --bitscore-selection {params.bitscore_selection} \
            {params.protein_id_is_gene_id_option} \
            --protein-id-to-gene-id "{input.mapping}" > "{log.stdout}" 2> "{log.stderr}"
        """
