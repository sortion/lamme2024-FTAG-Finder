"""
Yet another alternative for blastp computing, the good old blastall.

(We would rather recommend to use the newer blastp of ncbi-blast+)

Known short coming: formatdb creates blast db in the folder of input_pep_fasta
"""

if alignment_method == "blastall":

    rule blastall_blastp:
        input:
            query=input_pep_fasta,
            db=expand("{name}{ext}", name=input_pep_fasta, ext=[".phr", ".pin", ".psq"]),
        output:
            tsv="results/blastall/{name}.blastp.tsv",
        params:
            filter_complexity="T" if config["blastall"]["seg"] else "T",
            matrix=config["blastall"]["matrix"],
            evalue=config["blastall"]["evalue"],
            word_size=config["blastall"]["word_size"],
            gapopen=config["blastall"]["gapopen"],
            gapextend=config["blastall"]["gapextend"],
            hit_extension_threshold=config["blastall"]["hit_extension_threshold"],
            xdropoff=config["blastall"]["xdropoff"],
        threads: 6
        conda:
            "../envs/blast-legacy.yaml"
        log:
            stderr="logs/blastall_blastp/{name}.stdout",
            stdout="logs/blastall_blastp/{name}.stderr",
        shell:
            """
            blastall -p blastp \
                -d "{input.query}" -i "{input.query}" \
                -o "{output.tsv}" \
                -m 8 \
                -F "{params.filter_complexity}" \
                -e "{params.evalue}" \
                -M "{params.matrix}" \
                -W "{params.word_size}" \
                -G "{params.gapopen}" \
                -E "{params.gapextend}"\
                -f "{params.hit_extension_threshold}" \
                -y "{params.xdropoff}" \
                -a "{threads}" \
                > "{log.stdout}" 2> "{log.stderr}"
            """

    # should I add the other extensions to the outputs?
    rule formatdb:
        input:
            fasta=input_pep_fasta,
        output:
            multiext(input_pep_fasta, ".phr", ".pin", ".psq"),
        conda:
            "../envs/blast-legacy.yaml"
        log:
            stdout=f"logs/formatdb/{run_name}.stdout",
            stderr=f"logs/formatdb/{run_name}.stderr",
        shell:
            """
           formatdb -i "{input.fasta}" -o T -p T > "{log.stdout}" 2> "{log.stderr}"
           """
