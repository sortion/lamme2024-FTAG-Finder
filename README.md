# FTAG-Finder

[![Snakemake](https://img.shields.io/badge/snakemake-≥6.3.0-brightgreen.svg)](https://snakemake.github.io) <!-- TODO: update the minimum Snakemake version required. -->
<!--[![GitHub actions status](https://github.com/samuelortion/FTAG-Finder.smk/workflows/Tests/badge.svg?branch=main)](https://github.com/samuelortion/FTAG-Finder.smk/actions?query=branch%3Amain+workflow%3ATests)-->

A workflow for Gene Families and Tandemly Arrayed Genes detection.

We try our best to support both a Snakemake and a Galaxy powered version of the FTAG-Finder workflow.

<!-- The Snakemake version is available in the `snakemake` branch of this repository, whereas the Galaxy version is in the `galaxy` branch. -->

## Setup

### Installing dependencies manually

To get the workflow quickly setup, with (hopefully) tested software versions and environment, it is recommended to use a conda environment. <!-- TODO: or Docker / Singularity / what else, when it will be available (?) -->
<!-- TODO: add a global environment.yaml file with all required dependencies to be installed all in one environment, if possible -->

Otherwise, you can also install the dependencies on your own.

The workflow requires the following dependencies:

- a protein alignment method: `blastp` or any of the other supported alignment methods, which outputs a BLAST-like tsv output:
  - ncbi-blast+
  - blastall
  - mmseqs2 <!--TODO ensure this method is properly supported. -->, or
  - diamond
- a graph clustering method
  - mcl, or
  - R (with igraph >= 1.0 <!-- TODO: check whether the igraph<=2.0 works indeed. -->), if you chose the 'enhanced' Walktrap clustering method (which allows you to set the desired gene vertex communities _density_)
- python3 >= 3.9 (at least, the script have been developed with python3.12) <!-- TODO: matrix test on several python versions ? -->
- biopython


<!-- TODO: provide an INSTALL.md detailed example instruction set for expected standard Linux (/MacOS ?) distributions  -->

## Usage

<!-- The usage of this workflow is described in the [Snakemake Workflow Catalog](https://snakemake.github.io/snakemake-workflow-catalog/?usage=samuelortion%2FFTAG-Finder.smk). -->

<!-- If you use this workflow in a paper, don't forget to give credits to the authors by citing the URL of this (original) FTAG-Finder.smksitory and its DOI (see above). -->

### Run all

```bash
snakemake --snakefile=workflow/Snakefile --cores 4
```

### Running step by step

## Test dataset

A small bash script is provided to download test dataset (Ensemble TAIR10 proteome FASTA and GFF3 files) (requires `wget`).
You might want to download it on your own, from the [Ensembl plants portal](https://plants.ensembl.org/Arabidopsis_thaliana/Info/Index).

```bash
cd .test/data
bash download.sh
```

For faster end-to-end testing of the workflow, we provide a simple awk script to extract the first 1000 fasta records from the proteome.

## Releases convention

Uses [semver](https://semver.org/).

Label each major version with a code name [`<Adjective> <name>`](https://w.wiki/9y9h) where adjective is an epithet in English and genus is the vernacular name of a Bird species, starting with the same letter as the adjective.
