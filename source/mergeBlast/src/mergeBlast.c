#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/********************************************************************************
 * 4 august 2004
 * Author : Loic Ponger
 * Modifications : Bérengère Bouillon
 * selects the non-overlapping blast hits
 * SYNOPSIS
 *	 $0 filename error infofile outputfile
 * OPTIONS
 *	 -filename: input file containing blast output in -m8 format
 *	 -the file have to be SORTED according to the eValues or reverseSORTED
 *
 *       the bitScores
 *	 -error: number of overlapping aa allowed
 *   -infofile: output file for selected blast HSP
 *	 -outputfile: output file containing merge blast HSPs
 * OUTPUT
 *	 -filename.matchs including all the selected blast hits
 *	 -filename.pairs summaring the information for each pairs of proteins
 *		average similarity, cumulative alignment lentgh, ...
 ********************************************************************************/

#define LGMAXSEQNAME 50
#define LGMAXKEY 100 // 2 times  LGMAXSEQNAME
#define LGMAXINPUTFILENAME 100
#define LGMAXOUTPUTFILENAME 130 // LGMAXINPUTFILENAME + max(length(EXTMATCH), length(EXTPAIR))

/* Structural informations about each blast match (headers) */
struct infos
{
    char nameA[LGMAXSEQNAME];
    char nameB[LGMAXSEQNAME];
    float sim;
    int lgAlign;
    int mismatches;
    int gap;
    int beginA;
    int endA;
    int beginB;
    int endB;
    float eValue;
    float bitS;
    char key[LGMAXKEY];
};

/* Structural informations for sequence pairs (group of several matchs belonging
 * to the same gene pair) */
struct infosPairs
{
    char nameA[LGMAXSEQNAME];
    char nameB[LGMAXSEQNAME];
    char key[LGMAXKEY];
    int matchnb;
    float simcumul;
    int lgcumul;
    int lgcumulA;
    int lgcumulB;
    float eValue;
    float bitS;
};

/* Functions used */
void printtable(int nbligne, int *nbgenes, int *tabElimination, char ***table);
void *monrealloc(void *p,
                 size_t s); // to print an error message and stop the program
void copystructinfos(struct infos *st1, struct infos st2);
void copystructinfospairs(struct infosPairs *st1, struct infosPairs st2);
void printinfos(struct infos st);
void fprintinfos(FILE *f, struct infos st);
void printinfospairs(struct infosPairs st);
void fprintinfospairs(FILE *f, struct infosPairs st);
void copystructinfosininfospairs(struct infosPairs *st1, struct infos st2);

int main(int argc, char *argv[])
{

    /* Variables */
    FILE *f1, *f2, *f3; // f1 :filename; f2:filename.matchs; f3:filename.pairs
    char word[2000];
    char filenameM[LGMAXOUTPUTFILENAME] = "";
    char filenameP[LGMAXOUTPUTFILENAME] = "";
    char nameIn[LGMAXINPUTFILENAME];
    int ii, jj, nb, nbp, err;
    int smalloverlapA, smalloverlapB, ok;
    struct infos *mtch;
    struct infosPairs *pairs;
    struct infos readL;
    int d, f, m, compt = 0;
    int sameNameANameB = 0;
    float bitSPrevious, eValuePrevious;

    f1 = fopen(argv[1], "r");
    err = atoi(argv[2]); // for transformation in integer
    f2 = fopen(argv[3], "w");
    f3 = fopen(argv[4], "w");

    fprintf(f2, "nameA\tnameB\tsimil\tlgAlign\tnbMismtch\tnbGap\tbeginA\tendA\t"
                "beginB\tendB\teValue\tbitScore\n");

    /* Realloc with NULL equivalent to a malloc */
    mtch = NULL;
    pairs = NULL;

    nb = 0;  // number of matches
    nbp = 0; // number of pairs

    short int header = 1;
    if (header) {
        fgets(word, 2000, f1); // Skip header
    }

    /* Read the input file */
    while (fgets(word, 2000, f1) != NULL)
    {
        d = 0;
        f = nb;
        smalloverlapA = 0;
        smalloverlapB = 0;
        ok = 1;

        sscanf(word, "%s%s%f%d%d%d%d%d%d%d%e%f", readL.nameA, readL.nameB,
               &readL.sim, &readL.lgAlign, &readL.mismatches, &readL.gap,
               &readL.beginA, &readL.endA, &readL.beginB, &readL.endB,
               &readL.eValue, &readL.bitS); // read the input
        strcpy(readL.key, readL.nameA);     // record the first seq id of the line
        strcat(readL.key, readL.nameB);     // add the second seq id of the line

        if (sameNameANameB == 0 && strcmp(readL.nameA, readL.nameB) == 0)
            sameNameANameB = 1;

        compt++;

        bitSPrevious = readL.bitS;
        eValuePrevious = readL.eValue;

        /* Dichotomy */
        while ((nb > 0) && ((f - d) != 0))
        {
            m = (f + d) / 2;
            if (strcmp(readL.key, mtch[m].key) <= 0)
                f = m;
            else
                d = m + 1;
            if (d > f)
                f = d;
        }

        /* Overlap */
        /* On compare chaque ligne à toutes les autres présentant une dichotomie
         * (dans d).  */
        ii = d;
        while (ii < nb && strcmp(readL.key, mtch[ii].key) ==
                              0) // tant que les id des deux lignes sont égaux,
        {
            // tant que les matchs ne s'overlappent pas plus qu'autorisé :
            // pour le même id : readL -> ligne actuelle ; mtch[ii] -> ligne
            // suivante ok sera =0 et on supprimera la ligne
            if (readL.beginA <= (mtch[ii].endA - err) &&
                readL.endA >= (mtch[ii].beginA + err))
                ok = 0;
            if (readL.beginB <= (mtch[ii].endB - err) &&
                readL.endB >= (mtch[ii].beginB + err))
                ok = 0;
            if (ok == 1)
            {
                if (readL.beginA <= mtch[ii].endA &&
                    readL.endA >= mtch[ii].endA)
                    smalloverlapA += mtch[ii].endA - readL.beginA + 1;
                if (readL.endA >= mtch[ii].beginA &&
                    readL.beginA <= mtch[ii].beginA)
                    smalloverlapA += readL.endA - mtch[ii].beginA + 1;

                if (readL.beginB <= mtch[ii].endB &&
                    readL.endB >= mtch[ii].endB)
                    smalloverlapB += mtch[ii].endB - readL.beginB + 1;
                if (readL.endB >= mtch[ii].beginB &&
                    readL.beginB <= mtch[ii].beginB)
                    smalloverlapB += readL.endB - mtch[ii].beginB + 1;
            }
            ii++;
        }

        if (ok == 1) // if line is ok
        {
            fprintinfos(f2, readL); // write informations of actual line in the
                                    // output file f2

            mtch = (struct infos *)monrealloc(mtch,
                                              (nb + 1) * sizeof(struct infos));

            for (jj = nb; jj > d; jj--)
            {
                copystructinfos(&mtch[jj], mtch[jj - 1]);
            }
            nb++;
            copystructinfos(&mtch[d], readL);

            /* Dichotomie for pairs */
            d = 0;
            f = nbp;

            while ((nbp > 0) && ((f - d) != 0))
            {
                m = (f + d) / 2;
                if (strcmp(readL.key, pairs[m].key) <= 0)
                    f = m;
                else
                    d = m + 1;
                if (d > f)
                    f = d;
            }

            // if not found (new pair)
            // attention to the test in which d must be different
            // from nbp, otherwise there is segmentation fault
            if (nbp > 0 && d < nbp && strcmp(readL.key, pairs[d].key) == 0)
            {
                pairs[d].matchnb++;
                pairs[d].simcumul = (readL.sim * readL.lgAlign +
                                     pairs[d].lgcumul * pairs[d].simcumul) /
                                    (readL.lgAlign + pairs[d].lgcumul);
                pairs[d].lgcumul += readL.lgAlign;
                pairs[d].lgcumulA +=
                    (readL.endA - readL.beginA + 1) - smalloverlapA;
                pairs[d].lgcumulB +=
                    (readL.endB - readL.beginB + 1) - smalloverlapB;
            }
            else
            {

                pairs = (struct infosPairs *)monrealloc(
                    pairs, (nbp + 1) * sizeof(struct infosPairs));

                for (jj = nbp; jj > d; jj--)
                {
                    copystructinfospairs(&pairs[jj], pairs[jj - 1]);
                }
                copystructinfosininfospairs(&pairs[d], readL);

                nbp++;
            }
        }
    }
    fclose(f1);
    fclose(f2);

    fprintf(f3, "nameA\tnameB\tnbNonOverlapAlign\tsimilCumul\tlgCumul\tlgCumulA"
                "\tlgCumulB\tbestEValue\tbestBitScore\n");
    for (ii = 0; ii < nbp; ii++)
        fprintinfospairs(f3, pairs[ii]);
    fclose(f3);

    printf("-----\n");

    // if (sameNameANameB == 1)
    //	printf("-----\nWARNING: some lines with nameA==nameB.\n");

    printf("-----\nsimilCumul:\taverage of hit similarities weighted by hit "
           "length.\n");
    printf("lgCumul:\tsum of hit length.\n");
    printf("bitScore:\tbest bit scores.\n");
    printf("-----\nNormal end of the program\nIt was a plaesure to serve you "
           "!!!\nI hope to see you again  ;-)\n");

    return 0;
}

/* used to print an error message and stop the program */
void *monrealloc(void *p, size_t s)
{
    void *pp;

    if ((pp = (void *)realloc(p, s)) == NULL)
    {
        printf("ERROR: error of memory allocation\n");
        exit(1);
    }
    return pp;
}

void copystructinfos(struct infos *st1, struct infos st2)
{
    strcpy((*st1).nameA, st2.nameA);
    strcpy((*st1).nameB, st2.nameB);
    (*st1).sim = st2.sim;
    (*st1).lgAlign = st2.lgAlign;
    (*st1).mismatches = st2.mismatches;
    (*st1).gap = st2.gap;
    (*st1).beginA = st2.beginA;
    (*st1).endA = st2.endA;
    (*st1).beginB = st2.beginB;
    (*st1).endB = st2.endB;
    (*st1).eValue = st2.eValue;
    (*st1).bitS = st2.bitS;
    strcpy((*st1).key, st2.key);
}

void copystructinfospairs(struct infosPairs *st1, struct infosPairs st2)
{
    strcpy((*st1).nameA, st2.nameA);
    strcpy((*st1).nameB, st2.nameB);
    strcpy((*st1).key, st2.key);
    (*st1).matchnb = st2.matchnb;
    (*st1).simcumul = st2.simcumul;
    (*st1).lgcumul = st2.lgcumul;
    (*st1).lgcumulA = st2.lgcumulA;
    (*st1).lgcumulB = st2.lgcumulB;
    (*st1).eValue = st2.eValue;
    (*st1).bitS = st2.bitS;
}

void printinfos(struct infos st)
{
    printf("%s\t%s\t%f\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%e\t%f\n", st.nameA,
           st.nameB, st.sim, st.lgAlign, st.mismatches, st.gap, st.beginA,
           st.endA, st.beginB, st.endB, st.eValue, st.bitS);
}

void fprintinfos(FILE *f, struct infos st)
{
    fprintf(f, "%s\t%s\t%f\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%e\t%f\n", st.nameA,
            st.nameB, st.sim, st.lgAlign, st.mismatches, st.gap, st.beginA,
            st.endA, st.beginB, st.endB, st.eValue, st.bitS);
}

void printinfospairs(struct infosPairs st)
{
    printf("%s\t%s\t%d\t%f\t%d\t%d\t%d\t%e\t%f\n", st.nameA, st.nameB,
           st.matchnb, st.simcumul, st.lgcumul, st.lgcumulA, st.lgcumulB,
           st.eValue, st.bitS);
}

void fprintinfospairs(FILE *f, struct infosPairs st)
{
    fprintf(f, "%s\t%s\t%d\t%f\t%d\t%d\t%d\t%e\t%f\n", st.nameA, st.nameB,
            st.matchnb, st.simcumul, st.lgcumul, st.lgcumulA, st.lgcumulB,
            st.eValue, st.bitS);
}

void copystructinfosininfospairs(struct infosPairs *st1, struct infos st2)
{
    strcpy((*st1).nameA, st2.nameA);
    strcpy((*st1).nameB, st2.nameB);
    strcpy((*st1).key, st2.key);
    (*st1).matchnb = 1;
    (*st1).simcumul = st2.sim;
    (*st1).lgcumul = st2.lgAlign;
    (*st1).lgcumulA = st2.endA - st2.beginA + 1;
    (*st1).lgcumulB = st2.endB - st2.beginB + 1;
    (*st1).eValue = st2.eValue;
    (*st1).bitS = st2.bitS;
}
