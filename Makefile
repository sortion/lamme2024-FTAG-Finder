all: run

run:
	snakemake --snakefile=./workflow/Snakefile --cores=1 --software-deployment-method conda --use-conda --verbose

test:
	cd .test && pwd && snakemake --snakefile=../workflow/Snakefile \
		--software-deployment-method conda --show-failed-logs --cores 3 --conda-cleanup-pkgs cache --all-temp --configfile "config/config.yaml"

graphs: dag rulegraph filegraph

dag:
	snakemake -F --dag | dot -Tsvg > resources/dag.svg

rulegraph:
	snakemake -F --rulegraph | dot -Tsvg > resources/rulegraph.svg

filegraph:
	snakemake -F --filegraph | dot -Tsvg > resources/filegraph.svg
