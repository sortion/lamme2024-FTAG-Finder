#!/usr/bin/env bats

@test "Test list TAG_genes_pairs.py" {
    run python3 ../workflow/scripts/list/TAG_genes_pairs.py \
        --input sample/TAGs_example.tsv \
        --output tmp/TAG_genes_pairs_example.tsv \
        --definition 0,1,5,10
    [ "$status" -eq 0 ]
}
