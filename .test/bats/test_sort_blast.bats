#!/usr/bin/env bats

@test "Test sort_blast basic" {
    run python3 workflow/scripts/sort_blast.py \
        --input=.test/data/TAIR10.first1000.blastp.tsv \
        --output=/tmp/TAIR10.first1000.sorted.blastp.tsv \
        --input-fasta=.test/data/TAIR10_pep_20101214.first1000.pep.faa \
        --no-protein-id-is-gene-id \
        --protein-id-to-gene-id=.test/data/TAIR10.first1000.iso2gene.tsv \
        --filter-selection=computed \
        --isoform-alternative=longest
    [ "$status" -eq 0 ]
}
